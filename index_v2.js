const qrcode = require('qrcode-terminal');

const { Client,Buttons, LocalAuth, List } = require('whatsapp-web.js');

// const client = new Client({
//     authStrategy: new LocalAuth()
// });

const client = new Client({
    puppeteer: {
      args: ['--no-sandbox', "--disable-setuid-sandbox"]
    },
    authStrategy: new LocalAuth()
  });

client.on('qr', qr => {
    qrcode.generate(qr, {small: true});
});

client.on('ready', () => {
    console.log('Client is ready!');
});

client.on('message', async message => {
	if(message.body === '0' || message.body === 'Hi' || message.body === 'hi' || message.body === 'hello' || message.body === 'Hello' || message.body === 'hie' || message.body === 'Hie' || message.body === 'help' || message.body === 'Help')  {
        
     const mainMenu = new List(
          "Your health is our highest priority. We are here to give you vital reproductive health care information and sex education. Please reply with one of the following options. 🤗🤗😀",
          "Main menu",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "1", title: "Birth Control" },
                      { id: "2", title: "Cancer" },
                      { id: "3", title: "Pregnancy" },
                      { id: "4", title: "Relationships" },
                      { id: "5", title: "Rape" },
                      { id: "6", title: "STDs" },
                      { id: "7", title: "PrEP" },
                      { id: "8", title: "Get Tested" },
                  ],
              },
          ],
          "*Welcome to Eduspace !!!*"
      );
      message.reply(mainMenu);
     
     }

    /////////////////////////////////    Start of Birth Control    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "1" || message.body === 'Birth Control' || message.body === 'birth control'){
        const birthcontrolMenu = new List(
          "Birth control is how you prevent pregnancy. There are lots of different birth control options out there. We’re here to help you figure it all out.",
          "Birth controll menu",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "11", title: "Birth Control Implant" },
                      { id: "12", title: "Loop/IUD" },
                      { id: "13", title: "Depo/ Birth Control Shot" },
                      { id: "14", title: "Birth Control Pill" },
                      { id: "15", title: "Condom" },
                  ],
              },
          ],
          "*Birth Control*"
      );
      message.reply(birthcontrolMenu);

             }
             else if(message.body === "11" || message.body === 'Birth Control Implant' || message.body === 'birth control implant')
             {
                message.reply('*What is the birth control implant?*\nThe birth control implant is a tiny, thin rod about the size of a matchstick. The implant releases hormones into your body that prevent you from getting pregnant. A nurse or doctor inserts the implant into your arm and that’s it — you’re protected from pregnancy for up to 5 years. It’s get-it-and-forget-it birth control.\n\n*How does the implant work?*\nThe hormones in the birth control implant prevent pregnancy in two ways:\n- Progestin thickens the mucus on your cervix, which stops sperm from swimming through to your egg. When sperm can’t meet up with an egg, pregnancy can’t happen.\n- Progestin can also stop eggs from leaving your ovaries (called ovulation), so there’s no egg to fertilise. When eggs aren’t released, you can’t get pregnant.\n\nOne of the awesome things about the implant is that it lasts for a long time — up to 5 years — but it’s not permanent. If you decide you want to get pregnant or you just don’t want to have your implant anymore, your doctor can take it out.\n\nDoes the implant prevent STDs?\nNope. implant doesn`t protect against STDs. Luckily, using condoms or internal condoms every time you have sex does lower your chances of getting or spreading STDs. So using condoms with your implant is the best way to prevent infections.\n\n🔙 0. To go back to main menu');
                     }

            else if(message.body === "12" || message.body === 'Loop/IUD' || message.body === 'loop' || message.body === 'Loop' || message.body === 'IUD' || message.body === 'iud')
             {
                message.reply('*What’s an IUD/Loop?*\n\nAn loop is a tiny device thats put into your uterus to prevent pregnancy. It’s long-term, reversible, and one of the most effective birth control methods out there.\n\nThese Loops are divided into 2 types:hormonal loops (Mirena, Kyleena, Liletta, and Skyla).copper loops (Paragard) and The Paragard loop doesn’t have hormones. It’s wrapped in a tiny bit of copper, and it protects you from pregnancy for up to 12 years. The Mirena, Kyleena, Liletta, and Skyla loops use the hormone progestin to prevent pregnancy. Progestin is very similar to the hormone progesterone that our bodies make naturally. Mirena works for up to 8 years. Kyleena works for up to 5 years. Liletta works for up to 8 years. Skyla works for up to 3 years.\n\n*How do IUDs work?*\nBoth copper loops and hormonal loops prevent pregnancy by changing the way sperm cells move so they cant get to an egg. If sperm can’t make it to an egg, pregnancy can’t happen.\nThe Paragard loop uses copper to prevent pregnancy. Sperm doesn’t like copper, so the Paragard loop makes it almost impossible for sperm to get to that egg.\n\n*Can IUDs be used as emergency contraception?*\nYes! The Paragard, Mirena, and Liletta IUDs work super well as emergency contraception. If you get one of these IUDs put in within 120 hours (5 days) after unprotected sex, it’s more than 99% effective. It’s actually the most effective way to prevent pregnancy after sex.\n\n🔙 0. To go back to main menu');
                     }
             else if(message.body === "13" || message.body === 'Depo/ Birth Control Shot' || message.body === 'Depo' || message.body === 'depo' || message.body === 'Birth Control Shot' || message.body === 'birth control shot')
             {
                message.reply('*What is the birth control shot?*\n\nThe depo shot (AKA Depo-Provera) is an injection you get once every 3 months. It’s a safe, convenient, and private birth control method that works really well if you always get it on time.\n\n*How does the birth control shot work?*\nThe birth control shot (sometimes called Depo-Provera, the Depo shot, or DMPA) contains the hormone progestin.  Progestin stops you from getting pregnant by preventing ovulation.  When there’s no egg in the tube, pregnancy can’t happen. It also works by making cervical mucus thicker. When the mucus on the cervix is thicker, the sperm can’t get through. And when the sperm and the egg can’t get together, pregnancy can’t happen.\n\n*Does the shot protect against STDs?*\nNo. The shot is really good at preventing pregnancy, but it won’t protect you from sexually transmitted infections.\nLuckily, using condoms every time you have sex really lowers the chance of getting or spreading STDs. The other great thing about condoms is that they also protect against pregnancy, which means that using condoms along with the shot gives you awesome pregnancy-preventing power!\n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "14" || message.body === 'Birth Control Pill' || message.body === 'Birth control pill' || message.body === 'birth control pill' || message.body === 'pill' || message.body === 'Pill')
             {
                message.reply('*What is the birth control pill?*\n\nBirth control pills are a kind of medicine with hormones. Birth control pills come in a pack, and you take 1 pill every day. The pill is safe, affordable, and effective if you always take your pill on time. Besides preventing pregnancy, the pill has lots of other health benefits, too.\n\n*How do birth control pills prevent pregnancy?*\nThe birth control pill works by stopping sperm from joining with an egg. When sperm joins with an egg it’s called fertilisation. The hormones in the pill safely stop ovulation. No ovulation means there’s no egg for sperm to fertilise, so pregnancy can’t happen. The pill’s hormones also thicken the mucus on the cervix. This thicker cervical mucus blocks sperm so it can’t swim to an egg — kind of like a sticky security guard. \n\n*How do I make the pill work best for me?*\nForgetting pills, losing the pack, not refilling your prescription on time — these are the main reasons why people might get pregnant when they use the pill. It’s good to plan ahead and think about the best way for you to use the pill correctly. \n- Use our birth control reminder app or set an alarm on your phone.\n- Keep your pills in your bag so they’re always with you.\n- Your partner can help remind you.\n\n*Does the pill protect against STDs?*\nNope. The pill is really good at preventing pregnancy, but it won’t protect you from sexually transmitted infections. Using condoms every time lowers your chances of getting or spreading STDs.\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "15" || message.body === 'Condom' || message.body === 'condom')
             {
                message.reply('*What is a condom?*\n\nCondoms are thin, stretchy pouches that you wear on your penis during sex. Condoms provide great protection from both pregnancy and STDs. They’re easy to use and easy to get.\n\n*What’s a condom and how does it work?*\nCondoms are small, thin pouches that cover your penis during sex and collect semen (cum). Condoms prevent pregnancy by stopping sperm from getting into the vagina, so sperm can’t meet up with an egg.\nAlong with helping to prevent pregnancy, latex and plastic condoms also help prevent STDs by covering the penis — this prevents contact with semen and vaginal fluids, and limits skin-to-skin contact that can spread sexually transmitted infections.\n\nDo condoms help protect against STDs?\nYes! Using condoms every time you have oral or vaginal sex is the best way to reduce your chances of getting or spreading sexually transmitted infections. Condoms protect you and your partners from STDs by preventing contact with bodily fluids (like semen and vaginal fluids) that can carry infections. And because condoms cover your penis, they help protect against certain STDs like herpes and genital warts that are spread through skin-to-skin contact (but they’re somewhat less effective with these because they don’t cover all your skin).\n\nCondoms are the only type of birth control out there that also help protect against STDs. So even if you’re using another form of birth control (like the pill), it’s a good idea to also use condoms to prevent the spread of sexually transmitted infections.\n\n🔙 0. To go back to main menu');
                     }
    /////////////////////    End of Birth Control   \\\\\\\\\\\\\\\\\\\\\\\\


    /////////////////////////////////          Start of Cancer          \\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "2" || message.body === 'Cancer' || message.body === 'cancer'){
        const cancerMenu = new List(
          "There are many different types of cancer, including cancers that affect your breasts and genitals. Cancer can often be cured if you find it early and get treatment — that’s why it’s so important to get regular checkups and cancer screenings. Here’s more info on certain types of reproductive cancers and how to stay healthy.",
          "Cancer menu",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "20", title: "Cervical Cancer" },
                      { id: "21", title: "Breast Cancer" },
                  ],
              },
          ],
          "*Cancer*"
      );
      message.reply(cancerMenu);

            }
            else if(message.body === "20" || message.body === 'Cervical Cancer' || message.body === 'cervical cancer')
             {
                message.reply('*What’s cervical cancer?*\n\nCervical cancer is cancer of the cervix. The cervix is the lower, narrow opening of the uterus. It leads from your uterus to your vagina. Your cervix looks kind of like a donut if you look at it through your vagina.\nCervical cancer usually takes years to develop. During this time, the cells in the cervix change and grow rapidly. The early changes that happen before it becomes full blown cancer (precancerous) are called “dysplasia” or “cervical intraepithelial neoplasia” (CIN). If these changes are found and treated, cervical cancer can be prevented. If not diagnosed and treated, cervical cancer can spread to other parts of the body and become deadly.\n\n*Who’s at risk for cervical cancer?*\n- A personal history of dysplasia of the cervix, vagina, or vulva\n- A family history of cervical cancer\n- Smoking\n- Other infections such as chlamydia\n- Immune system problems such as HIV/AIDS that make it harder to fight infections like HPV\n- Having a mother who took a drug called diethylstilbestrol (DES) during pregnancy\n- Age is also a factor. The average age that cervical cancer is diagnosed is 48. It rarely affects those younger than 20.\n\n*What can I do to prevent cervical cancer?*\n\nGet regular wellness exams that include HPV and/or Pap tests when you need them. Your doctor or nurse can help you figure out which test(s) make sense for you, and when you should start getting screened.\n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "21" || message.body === 'Breast Cancer' || message.body === 'breast cancer')
             {
                message.reply('*What’s cervical cancer?*\n\nCervical cancer is cancer of the cervix. The cervix is the lower, narrow opening of the uterus. It leads from your uterus to your vagina. Your cervix looks kind of like a donut if you look at it through your vagina.\nCervical cancer usually takes years to develop. During this time, the cells in the cervix change and grow rapidly. The early changes that happen before it becomes full blown cancer (precancerous) are called “dysplasia” or “cervical intraepithelial neoplasia” (CIN). If these changes are found and treated, cervical cancer can be prevented. If not diagnosed and treated, cervical cancer can spread to other parts of the body and become deadly.\n\n*Who’s at risk for cervical cancer?*\n- A personal history of dysplasia of the cervix, vagina, or vulva\n- A family history of cervical cancer\n- Smoking\n- Other infections such as chlamydia\n- Immune system problems such as HIV/AIDS that make it harder to fight infections like HPV\n- Having a mother who took a drug called diethylstilbestrol (DES) during pregnancy\n- Age is also a factor. The average age that cervical cancer is diagnosed is 48. It rarely affects those younger than 20.\n\n*What can I do to prevent cervical cancer?*\n\nGet regular wellness exams that include HPV and/or Pap tests when you need them. Your doctor or nurse can help you figure out which test(s) make sense for you, and when you should start getting screened.\n\n🔙 0. To go back to main menu');
                     }
    ///////////////////////////////               End of Cancer                  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    //////////////////////////////                Start of Pregnancy              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "3" || message.body === 'Pregnancy' || message.body === 'pregnancy'){
        message.reply('Maybe you want the facts on how pregnancy happens. Or maybe you’re pregnant and want to know more about your options. Or maybe you want to know how to have a healthy pregnancy. You’ve come to the right place.\n\n🤰🏽30. Considering Pregnancy\n🧪 31. Pregnancy Tests\n🙇🏽‍♀️ 32. Miscarriage\n🫂 33. Pregnancy Symptoms\n🔙 0. To go back to main menu');

        const pregnancyMenu = new List(
          "Maybe you want the facts on how pregnancy happens. Or maybe you’re pregnant and want to know more about your options. Or maybe you want to know how to have a healthy pregnancy. You’ve come to the right place.",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "30", title: "Considering Pregnancy" },
                      { id: "31", title: "Pregnancy Tests" },
                      { id: "32", title: "Miscarriage" },
                      { id: "33", title: "Pregnancy Symptoms" },
                  ],
              },
          ],
          "*Pregnancy*"
      );
      message.reply(pregnancyMenu);

             }
            else if(message.body === "30" || message.body === 'Considering Pregnancy' || message.body === 'Considering pregnancy'  || message.body === 'considering pregnancy')
             {
                message.reply('*When is the right time to have a baby?*\n\nBecoming a parent is a big deal, and there’s a lot to think about. Everyone’s situation is different, but the decision is personal and only you can know when you’re ready.\n\n*When should I have a baby?*\nEven though a lot of people expect to recognise the “perfect time” to have a baby, the reality is the timing may never feel totally perfect.  But certain points in our lives are usually better than others to become a parent \n— like when you know you can meet your child’s needs and give them a loving and healthy home.\n- It’s normal to have lots of different feelings about whether you’re ready to take on the challenge of parenting. Here are some things to think about:\n- Am I ready to be totally responsible for all of my child’s needs?\n- Will I be able to raise my child in a safe and healthy environment?\n- Can I afford to raise a child right now?\n- Who will I raise a child with? How will we share the work of parenting?\n- How much support will I have from my family and friends?\n- What would having a baby right now mean for my future?\n- How would having a baby right now affect my family?\n- Is someone pressuring me to become a parent?\n- Am I ready to go through pregnancy and childbirth?\n- Will I be able to take care of my health and get prenatal care in order to have a healthy pregnancy and child?\nParenting is a lifelong commitment. So no matter when you decide to become a parent, you’ve got to be totally sure it’s what you want to do for a very long time.\n\n🔙 0. To go back to pregnancy menu');
                     }
            else if(message.body === "31" || message.body === 'Pregnancy Tests' || message.body === 'Pregnancy tests'  || message.body === 'pregnancy tests')
             {
                message.reply('*How do pregnancy tests work?*\n\nPregnancy tests are an easy and accurate way to find out if you’re pregnant — you just pee on a stick. They’re inexpensive and available at most drug and grocery stores.\n\n*How accurate are pregnancy tests?*\nPregnancy tests are super accurate when you use them correctly. The pregnancy tests you get at the drugstore work 99 out of 100 times. They’re just as accurate as a urine pregnancy test that you’d get at a doctor’s office. Pregnancy tests work by checking your urine (pee) for a hormone called human chorionic gonadotropin (HCG). Your body only makes this hormone if you’re pregnant.\n\n*How soon can I take a pregnancy test?*\nYou can take a pregnancy test anytime after your period is late — that’s when they work the best. It’s a good idea to take a pregnancy test as soon as possible if you miss your period or think you might be pregnant.\nThe earlier you know you’re pregnant, the sooner you can start thinking about your options and get whatever care you need to stay healthy.\n\n*Where can I get a pregnancy test?*\nYou can buy a pregnancy test at your local pharmacy, drugstore. Pregnancy tests are usually inexpensive — they can cost as little as a dollar. Sometimes you can get a free pregnancy test at certain health centres.You can also get a pregnancy test from your nurse or doctor, community clinic.\n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "32" || message.body === 'Miscarriage' || message.body === 'miscarriage')
             {
                message.reply('*What is a miscarriage?*\n\nMiscarriage is when an embryo or fetus dies before the 20th week of pregnancy. Miscarriage usually happens early in your pregnancy — 8 out of 10 miscarriages happen in the first 3 months.\n\n*What are the causes of miscarriages?*\nSome things that are known to cause miscarriages include:\n- Certain illnesses, like severe diabetes, can increase your chances of having a miscarriage.\n- A very serious infection or a major injury may cause miscarriage.\n- Late miscarriages — after 3 months — may be caused by abnormalities in the uterus.\n- If you’ve had more than 2 miscarriages in a row, you’re more likely to have a miscarriage.\n\n*What are the signs of miscarriage?*\nSometimes, there are no miscarriage symptoms and you don’t find out until an ultrasound, or you don’t feel pregnant anymore. Usually there are signs and symptoms. They include:\n- vaginal bleeding or spotting\n- severe belly pain\n- severe cramping\nOther things that are less serious than miscarriage can also cause these symptoms. But if you think you might be having a miscarriage, see your doctor right away just to be safe.\n\n*What happens during a miscarriage?*\nNot all miscarriages are physically painful, but most people have cramping. The cramps are really strong for some people, and light for others (like a period or less). It’s also common to have vaginal bleeding and to pass large blood clots up to the size of a lemon.\n The bleeding and cramping can end quickly, or it may last for several hours.\n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "33" || message.body === 'Pregnancy Symptoms' || message.body === 'pregnancy symptoms')
             {
                message.reply('*How do I know if I’m pregnant?*\n\nThe only way to know for sure if you’re pregnant is by taking a pregnancy test. You can go to your doctor’s office, or other community health centre to get a pregnancy test, or you can buy a pregnancy test at your local drugstore.\n\n*What are the early signs and symptoms of pregnancy?*\nFor most people, the first sign of pregnancy is a missed period. However, missing a period doesn’t always mean you’re pregnant. \n- Other early pregnancy symptoms include: \n- Slight bleeding or spotting\n- Tender or swollen breasts \n- Feeling tired\n- Feeling bloated\n- Urinating (peeing) more than usual\n- Mood swings\n- Nausea and/or vomiting\n\nNot everyone has all of these symptoms, but it’s common to have at least 1.Many of these symptoms can also be signs of other conditions and don’t always mean that you’re pregnant. Even missing a period isn’t always a clear sign, especially if you’re on hormonal birth control, which can change or even get rid of your period while you’re using it. That’s why the only way to know for sure if you’re pregnant is to take a pregnancy test. \n\nThe best way to prevent pregnancy is by using both birth control and condoms. \n\n🔙 0. To go back to pregnancy menu');
                     }
    //////////////////////////////                End of Pregnancy              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /////////////////////////////                 Start of relationships              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "4" || message.body === 'Relationships' || message.body === 'relationships'){

        const relationshipsMenu = new List(
          "Sexual and romantic relationships are an important part of life. No matter what your relationship status is, we’ll help you keep things healthy and safe. Relationships Are Complicated. Learn How to Handle Them.",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "40", title: "How can I have a good relationship?" },
                      { id: "41", title: "What makes a relationship unhealthy?" },
                      { id: "42", title: "How should I end a relationship?" },
                  ],
              },
          ],
          "*Relationships*"
      );
      message.reply(relationshipsMenu);

             }
             else if(message.body === "40" || message.body === "How can I have a good relationship?")
             {
                message.reply('*What are some tips for having a great relationship?*\n\nNo relationship is perfect all the time. But in a healthy relationship, both people feel good about the relationship most of the time. A great relationship takes more than attraction — it takes work. Here are some tips for building a healthy relationship:\n\n*Love yourself.* Being comfortable with who you are means you’ll be a happier partner.\n\n*Communicate.* Talk to your partner about your feelings. Ask questions and listen to their answers. If you’re upset, say so — don’t make your partner try to figure out what’s up. Talking through problems builds trust and makes your relationship stronger.\n\n*Be honest.* Be truthful with each other about what you do, think, and feel. Honesty creates trust. Few things harm a relationship more than lies.  \n\n*Give each other some space.* Couple’s time is great, but spending ALL your time together isn’t.\n\n*Agree to disagree.* You’re not always going to see eye to eye, and that’s OK. The important thing is to respect each other’s opinions and ideas.\n\n*Forgive and ask for forgiveness.* Everybody makes mistakes. Be willing to apologise for yours — and accept your partner’s apologies.\n\n*Support each other.* When your partner does something great, tell them!\n\n*Talk about sex…openly and honestly.* Telling your partner what feels good and what you like and don’t like helps you have better sex. \n\n*Take care of your sexual health.* Talk to your partner about how you’re going to protect each other against STDs and unintended pregnancy. Practice safer sex and get tested for STDs. \n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "41" || message.body === "What makes a relationship unhealthy?")
             {
                message.reply('*How do I know if my relationship is unhealthy?*\n\nLying, cheating, jealousy, and disrespect are signs of an unhealthy relationship. That includes:\n- keeping track of where they are and who they hang out with\n- checking their phone or e-mail without permission\n- keeping them away from friends or family\n- telling them they can’t do certain activities\n- preventing them from having money\n\n*What are the signs of an abusive relationship?*\nThere are different types of abuse that can affect your body, your emotions, and your self-esteem.\n- Physical abuse means hitting, kicking, pushing, or hurting someone in any way.\n- Sexual abuse or rape is forcing your partner to do anything sexual, from kissing to having sex.\n- Verbal abuse is name-calling, put-downs, and using words to hurt someone.\n- Emotional abuse is when your partner tries to make you feel bad about yourself. That can mean hurting your feelings on purpose, jealousy, blaming you for the abuse, cheating, or continually criticising you.\n- Reproductive control is pressuring your partner to get pregnant, end a pregnancy, lying about birth control, or other controlling decisions about pregnancy and parenting.\n- Threats and intimidation use the threat of violence or abuse to control a partner. Threatening children, suicide, or physical violence are all ways to control your behaviour.\n- Isolation is controlling who you see, what you do, and limiting your access to friends, family, and other forms of emotional and financial support.\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "42" || message.body === "How should I end a relationship?")
             {
                message.reply('Whats the best way to break up with someone?*\n\nRelationships end for a lot of reasons. Maybe you’re not happy with your partner, or maybe you just don’t want to be in a relationship right now. Whatever the reason, breaking up can be tough. These tips may help:\n\n*Prepare.* Think about what youre going to say in advance. You may even want to practice on a friend or in front of a mirror.\n\n*Pick the right spot.* Talk to your partner somewhere that’s comfortable for both of you. If you’re worried about safety, somewhere public might be the best choice.\n\n*Say it in person.* If you feel safe, talk to your partner face to face. E-mailing, texting, or talking on the phone may sound easier, but it’s usually not the best option. And don’t ask a friend to deliver the news for you.\n\n*Be respectful.* If your partner asks you why you’re breaking up with them, be honest — it could help them have better relationships in the future. But don’t insult them or try to hurt them.\n\n*Make a clean break.* If you really want to be friends, that’s fine. But if you’re just saying “let’s be friends” to let your partner down easier … don’t. It can lead to more hurt feelings. Even if you plan to stay friends, give your partner some space.\n\n*Stick with your decision.* If you feel like you’re doing the right thing, don’t let your partner try to convince you to stay together. It’s normal for someone to cry or get upset during a breakup, and that can be really hard to deal with. But feeling bad or guilty isn’t a reason to stay in a relationship.\n\n🔙 0. To go back to main menu');
                     }
    /////////////////////////////                 End of relationships              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /////////////////////////////                 Start of rape              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "5" || message.body === 'Rape' || message.body === 'rape'){

        const rapeMenu = new List(
          "Rape, sexual assault, and sexual abuse can have different legal definitions. In general, rape, sexual assault, and sexual abuse are forms of violence in which there is sexual contact without consent — including vaginal or anal penetration, oral sex, and genital touching.",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "50", title: "What is sexual consent?" },
                      { id: "51", title: "How do I talk about consent?" },
                      { id: "52", title: "What should I do if I was raped?" },
                  ],
              },
          ],
          "*Relationships*"
      );
      message.reply(rapeMenu);

             }
             else if(message.body === "50" || message.body === "What is sexual consent?")
             {
                message.reply('*What’s consent?*\n\nSexual consent is an agreement to participate in a sexual activity. Before being sexual with someone, you need to know if they want to be sexual with you too. It’s also important to be honest with your partner about what you want and don’t want.\n\nWithout consent, sexual activity (including oral sex, genital touching, and vaginal or anal penetration) is sexual assault or rape.\n\nYou get the final say over what happens with your body. It doesn’t matter if you’ve hooked up before or even if you said yes earlier and then changed your mind. You’re allowed to say “stop” at any time, and your partner needs to respect that.\n\nConsent is never implied by things like your past behaviour, what you wear, or where you go. Sexual consent is always clearly communicated — there should be no question or mystery. Silence is not consent. And it’s not just important the first time you’re with someone. Couples who’ve had sex before or even ones who’ve been together for a long time also need to consent before sex — every time.\n\nThere are laws about who can consent and who can’t. People who are drunk, high, or passed out can’t consent to sex. There are also laws to protect minors (people under the age of 18) from being pressured into sex with someone much older than them.\nThe age of sexual consent is how old a person needs to be in order to be considered legally capable of consenting to sex. Adults who have sex with someone younger than the age of consent face jail time and being registered as a sex offender.\n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "51" || message.body === "How do I talk about consent?")
             {
                message.reply('*How do I talk about consent?*\n\nTalking about what you want and don’t want and respecting your partner’s boundaries doesn’t have to be serious, scary, or awkward. Consenting and asking for consent means that everyone is being clear about their wants and needs — and that partners are respecting each other’s limits. Bonus: talking about what you want in bed can make you and your partner feel more turned-on and respected.\n\nSo, how do you ask for consent? It’s simple. Ask: “Can I [fill in the blank]?” or “Do you want me to do [fill in the blank]?” And listen for the answer. It’s also important to pay attention to their body language and tone.\nIf your partner says “yes” or makes it clear that they’re into it, then you have consent.\nIf your partner says “no,” doesn’t say anything, or says yes but seems unsure or uncomfortable, then you DON’T have consent.\nIf you don’t know what they want, or they say yes but don’t seem sure, check in before you continue. You can check in by saying something like:\n- “I want to make sure you want to do this. Should I keep going?”\n- “It’s okay if you’re not into this. We can do something else. What do you think?”\nNever, ever pressure your partner into something they don’t want to do or seem unsure about. Let them know it’s okay if they want to stop or do something different. And once you know someone isn’t into what you’re asking about, stop asking. Everyone deserves to have their boundaries respected.\n\n🔙 0. To go back to main menu');
                     }
            else if(message.body === "52" || message.body === "What should I do if I was raped?")
             {
                message.reply('*What should I do if I was raped?*\n\nRemember:\n\nIt’s not your fault. You may be feeling a range of emotions, but whatever you feel, know that what happened wasn’t your fault. It was 100% their fault. Don’t blame yourself.\n\nMake sure you’re safe. Get to a safe place or call a friend who can help you If the person who assaulted you is a family member or someone you know, tell someone you trust what happened.\n\nOnce you’re in a safe place, don’t do anything to change your appearance. It’s important that the doctor or nurse you visit can collect any evidence that might be on your body. So don’t take a shower or bath or wash off any parts of your body. Also if you can, don’t go to the bathroom, comb your hair, eat, smoke, drink or take any drugs. If you change your clothes, take the clothes you were wearing during the assault to the hospital or police department in a paper bag.\n\nGet medical care.\nIf you have injuries, you should go to the hospital right away.\n\nIf you’re worried about having been exposed to HIV, you can take a medicine called PEP (Post-Exposure Prophylaxis) which can help prevent getting HIV after being exposed. You need to start this treatment within 72 hours of being exposed.\n\nIf there’s a chance you could be pregnant, consider taking the morning-after pill, also known as emergency contraception.\n\nIf you’re worried about STDs, it’s a good idea to get tested.\n\nFind support. Dealing with the aftermath of rape or sexual assault can be overwhelming. But you’re not alone. It may help to talk to a trusted friend, family member, or counsellor.\n\n🔙 0. To go back to main menu');
                     }
    /////////////////////////////                 End of rape              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /////////////////////////////                 Start of STDs              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "6" || message.body === 'STDs' || message.body === 'stds' || message.body === 'std' || message.body === 'STD'){

        const stdsMenu = new List(
          "STDs are infections that are spread from one person to another, usually during vaginal and oral sex. They’re really common, and lots of people who have them don’t have any symptoms. Without treatment, STDs can lead to serious health problems. But the good news is that getting tested is no big deal, and most STDs are easy to treat.",
          [
              {
                  title: "Please select one of the options",
                  rows: [
                      { id: "60", title: "Genital warts" },
                      { id: "61", title: "Gonorrhoea" },
                      { id: "62", title: "Hepatitis B" },
                      { id: "63", title: "Herpes" },
                      { id: "64", title: "Syphilis" },
                      { id: "65", title: "HIV" },
                  ],
              },
          ],
          "*STDs/STIs*"
      );
      message.reply(stdsMenu);

             }
             else if(message.body === "60" || message.body === "Genital warts")
             {
                message.reply('*What are Genital Warts?*\nGenital warts are common and are caused by certain types of HPV. Genital warts can be annoying, but they’re treatable and aren’t dangerous.\n\n*Genital warts are caused by HPV*\nGenital warts show up on the skin around your genitals and anus. They’re caused by certain types of human papillomavirus (HPV). You might’ve heard that some types of HPV can cause cancer, but they’re NOT the same kinds that give you genital warts.\n\n*How do you get genital warts?*\nYou get genital warts from having skin-to-skin contact with someone who’s infected, often during vaginal and oral sex. Genital warts can be spread even if no one cums, and a penis doesn’t have to go inside a vagina to get them. You can spread them even when you don’t have any visible warts or other symptoms, though that’s less common. You can also pass genital warts to a baby during vaginal childbirth, but that’s pretty rare.\n\nGenital warts are different from warts you might get elsewhere on your body. So you can’t get genital warts by touching yourself (or a partner) with a wart that’s on your hand or foot.\n\nYou’re more likely to pass genital warts when you’re having symptoms. So if you notice a wart, it’s best to get tested and treated to help lower the risk of passing genital warts on to a partner.\n\n 🔙 0. To go back to main menu');
                     }
             else if(message.body === "61" || message.body === "Gonorrhoea" || message.body === "gonorrhoea")
             {
                message.reply('*What is Gonorrhea?*\nGonorrhea is a common bacterial infection that’s easily cured with antibiotic medicine. It’s sexually transmitted, and most people with gonorrhea don’t have symptoms.\n\n*One of the most common STDs*\nGonorrhea is a very common sexually transmitted infection, especially for teens and people in their 20s. Gonorrhea is sometimes called “the drip."\n\nGonorrhea is spread through vaginal oral sex. The infection is carried in semen (cum), pre-cum, and vaginal fluids. Gonorrhea can infect your penis, vagina, cervix, anus, urethra, throat, and eyes. Most people with gonorrhea don’t have any symptoms and feel totally fine, so they might not even know they’re infected.\n\nGonorrhea is usually easily cured with antibiotics. But if you don’t treat gonorrhea early enough, it can lead to more serious health problems in the future. That’s why STD testing is so important — the sooner you know you have gonorrhea, the faster you can get rid of it.\nYou can help prevent gonorrhea by using condoms every time you have sex.\n\n*How do you get gonorrhea?*\nPeople usually get gonorrhea from having unprotected sex with someone who has the infection. Gonorrhea is spread when semen (cum), pre-cum, and vaginal fluids get on or inside your genitals or mouth. Gonorrhea can be passed even if the penis doesn’t go all the way in the vagina.\n\nThe main ways people get gonorrhea are from having vaginal sex or oral sex. You can also get gonorrhea by touching your eye if you have infected fluids on your hand. Gonorrhea can also be spread to a baby during birth if the mother has it.\n🔙 0. To go back to main menu');
                     }
             else if(message.body === "62" || message.body === "Hepatitis B" || message.body === "hepatitis b")
             {
                message.reply('*What is Hepatitis B?*\n\nHepatitis B is an infection that can cause liver disease. It can be spread through sex. You can protect yourself by getting the hepatitis B vaccine and using condoms.\n\n*How to prevent hepatitis B*\nHepatitis B is a liver infection caused by a virus (called the hepatitis B virus, or HBV). It can be serious and there’s no cure, but the good news is it’s easy to prevent. You can protect yourself by getting the hepatitis B vaccine and having safer sex.\n\n*How do you get hepatitis B?*\nHepatitis B is really contagious. It’s transmitted through contact with semen (cum), vaginal fluids, and blood. You can get it from:\n- having vaginal or oral sex (using a condom or dental dam during sex can help prevent it)\n- sharing toothbrushes and razors (blood on them can carry hepatitis B)\n- sharing needles for shooting drugs, piercings, tattoos, etc.\n- getting stuck with a needle that has the Hep B virus on it.\n- Hepatitis B can also be passed to babies during birth if their mother has it.\n\nHepatitis B isn’t spread through saliva (spit), so you CAN’T get hepatitis B from sharing food or drinks or using the same fork or spoon. Hepatitis B is also not spread through kissing, hugging, holding hands, coughing, sneezing, or breastfeeding.\n\n🔙 0. To go back to main menu');
                     }
             else if(message.body === "63" || message.body === "Herpes" || message.body === "herpes")
             {
                message.reply('*What is herpes?*\n\nHerpes is a common virus that causes sores on your genitals and/or mouth. Herpes can be annoying and painful, but it usually doesn’t lead to serious health problems.\n\n*Herpes is a common infection.*\nHerpes is a super-common infection that stays in your body for life.Herpes is caused by two different but similar viruses: herpes simplex virus type 1 (HSV-1) and herpes simplex virus type 2 (HSV-2). Both kinds can make sores pop up on and around your vulva, vagina, cervix, anus, penis, scrotum, butt, inner thighs, lips, mouth, throat, and rarely, your eyes.\n\nHerpes is spread from skin-to-skin contact with infected areas, often during vaginal sex, oral sex and kissing. Herpes causes outbreaks of itchy, painful blisters or sores that come and go. Many people with herpes don’t notice the sores or mistake them for something else, so they might not know they’re infected. You can spread herpes even when you don’t have any sores or symptoms.\n\nThere’s no cure for herpes, but medication can ease your symptoms and lower your chances of giving the virus to other people. And the good news is, outbreaks usually become less frequent over time, and even though herpes can sometimes be uncomfortable and painful, it’s not dangerous. \n\n*How do you get herpes?*\nHerpes is easily spread from skin-to-skin contact with someone who has the virus. You can get it when your genitals and/or mouth touch their genitals and/or mouth — usually during oral and vaginal sex.\n\n🔙 0. To go back to STD menu');
                     }
             else if(message.body === "64" || message.body === "Syphilis" || message.body === "syphilis")
             {
                message.reply('*What is Syphilis?*\n\nSyphilis is a common bacterial infection that’s spread through sex. Syphilis is easily cured with antibiotic medicine, but it can cause permanent damage if you don’t get treated.\n\nSyphilis is serious — but it can be cured.\nSyphilis is a really common STD. Syphilis is spread through vaginal and oral sex.\n\nSyphilis causes sores on your genitals (called chancres). The sores are usually painless, but they can easily spread the infection to other people. You get syphilis from contact with the sores. A lot of people with syphilis don’t notice the sores and feel totally fine, so they might not know they have it.\n\nSyphilis can infect your vagina, anus, penis, or scrotum, and sometimes your lips and mouth. You can help prevent syphilis by using condoms every time you have sex.\n\nSyphilis can be easily cured with medication if you treat it early. But without treatment, it leads to really serious, permanent problems like brain damage, paralysis, and blindness. That’s why STD testing is so important — the sooner you know you have syphilis, the faster you can get rid of it.\n\n*How do you get syphilis?*\nSyphilis is spread from sexual skin-to-skin contact with someone who has it. You get it when your vulva, vagina, penis, anus, or mouth touches someone’s syphilis sores — usually during sex. Syphilis can be spread even if no one cums.\n\nSyphilis isn’t spread through casual contact, so you CAN’T get it from sharing food or drinks, hugging, holding hands, coughing, sneezing, sharing towels, or sitting on toilet seats.\n\n🔙 0. To go back to main menu');
                     }
             else if(message.body === "65" || message.body === "HIV & AIDS" || message.body === "HIV")
             {
                message.reply('*What is HIV?*\nHIV is the virus that causes AIDS. It damages your immune system, making it easier for you to get sick. HIV is spread during sex, but condoms can help protect you.\n\n*HIV is an infection that can lead to AIDS.*\nHIV stands for Human Immunodeficiency Virus. It’s a virus that breaks down certain cells in your immune system (your body’s defence against diseases that helps you stay healthy). When HIV damages your immune system, it’s easier to get really sick and even die from infections that your body could normally fight off.\n\n*What’s the difference between HIV and AIDS?*\nHIV is the virus that causes AIDS. AIDS stands for Acquired Immune Deficiency Syndrome. HIV and AIDS are not the same thing. And people with HIV do not always have AIDS.\n\nHIV is the virus that’s passed from person to person. Over time, HIV destroys an important kind of the cell in your immune system (called CD4 cells or T cells) that helps protect you from infections. When you don’t have enough of these CD4 cells, your body can’t fight off infections the way it normally can.\nAIDS is the disease caused by the damage that HIV does to your immune system.\n\n*How do you get HIV?*\nHIV is usually spread through having unprotected sex. Using condoms every time you have sex and not sharing needles can help protect you and your partners from HIV. If you do have HIV, treatment can lower or even stop the chances of spreading the virus to other people during sex. If you don’t have HIV, there’s also a daily medicine called PrEP that can protect you from HIV.\n\n🔙 0. To go back to STD menu');
                     }
    /////////////////////////////                 End of STDs              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /////////////////////////////                 Start of PrEP              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "7" || message.body === 'PrEP' || message.body === 'Prep' || message.body === 'prep' || message.body === 'PREP'){
        message.reply('*What is PrEP?*\nPrEP stands for pre-exposure prophylaxis. It’s a medicine that can help prevent HIV. Using PrEP can lower your chances of getting HIV from sex by up to 99%. \n\n*Who can use PrEP?*\nAnyone who is sexually active and doesnt have HIV can use PrEP.  To find out if PrEP is right for you, talk to your nurse or doctor. You may choose to use PrEP if you: \n\n- Have had anal or vaginal sex in the last 6 months and:\n- Have a sexual partner who has HIV\n- Don’t regularly use condoms\n- Have been diagnosed with another STD within the last 6 months.\n- Have shared needles, syringes, or other equipment to inject drugs in the last 6 months.\n- Have used PEP multiple times.\n\nIf you’re at high risk for HIV and you’re pregnant, trying to get pregnant, or breastfeeding, PrEP may also help you and your baby avoid getting HIV.\n\n*How effective is PrEP?*\nIf you use it correctly, PrEP can lower your chances of getting HIV from sex by up to 99% if taken correctly. And using condoms and PrEP together helps you stay even safer. \n\nIt’s really important to use PrEP correctly, meaning on time, every time. PrEP doesn’t work as well if you skip pills or miss your shot appointments. If you don’t take it on schedule, there might not be enough medicine in your body to block HIV. Use condoms along with PrEP to help you avoid other STDs and give you extra protection against HIV.\n\n*What are the side effects of PrEP?*\nPrEP is very safe. No serious problems have been reported in people who take PrEP.\n\n🔙 0. To go back to main menu');
             }
    /////////////////////////////                 End of PrEP              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    /////////////////////////////                 Start of Get Tested              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    else if(message.body === "8" || message.body === "Get Tested"){
        message.reply('*Should I get tested for STDs?*\nMost of the time, STDs have no symptoms. Testing is the only way to know for sure if you have an STD. So if you’ve had any kind of sexual contact that can spread STDs — like vaginal or oral sex — talk with a doctor or nurse about getting tested.\n\n*I think I have symptoms of an STD. Should I get tested?*\nIf you’ve had sexual contact with another person and notice any signs of an STD, talk to a doctor or nurse about getting tested. STD symptoms can come and go over time, but that doesn’t mean the STD is gone.\n\nDifferent STDs have different symptoms. Signs of STDs include:\n- sores or bumps on and around your genitals, thighs, or butt cheeks\n- weird discharge from your vagina or penis\n- burning when you pee and/or having to pee a lot\n- itching, pain, irritation and/or swelling in your penis, vagina, vulva, or anus\n- flu-like symptoms like fever, body aches, swollen glands, and feeling tired.\n\nAll of these symptoms can be caused by things that aren’t STDs (like pimples, UTIs, or yeast infections). So getting tested is the only way to know for sure what’s going on. Your nurse or doctor will help you figure out what kinds of testing or treatment you may need. Tell them about: \n\n- Your symptoms \n- What kind of sexual contact you’ve had (like vaginal or oral sex, or anything that involves skin-to-skin genital contact or passing sexual fluids)\n- Whether you use condoms and/or dental dams.\n\n🔙 0. To go back to main menu');
             }
    /////////////////////////////                 End of Get Tested              \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

});


client.initialize();
